from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.core.exceptions import ObjectDoesNotExist
from django.db import models


class UserManager(BaseUserManager):
    def create_user(self, username):
        user = self.model(username=username)
        user.save()
        return user

    def create_superuser(self, username):
        user = self.model(username=username)
        user.save()
        return user


class User(AbstractBaseUser):
    username = models.CharField(unique=True, null=False, max_length=30)

    REQUIRED_FIELDS = []
    USERNAME_FIELD = 'username'

    objects = UserManager()

    def get_group(self):
        try:
            return GroupMember.objects.get(active=True, user=self).group
        except ObjectDoesNotExist:
            return None

    def __str__(self):
        return self.username


class Group(models.Model):
    name = models.CharField(max_length=30, unique=True)
    password = models.CharField(max_length=30, null=True)

    def get_leader(self):
        return GroupMember.objects.filter(group=self, active=True).first().user


class GroupMember(models.Model):
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    active = models.BooleanField(default=True)


class Game(models.Model):
    startTime = models.DateTimeField(null=False)
    duration = models.IntegerField()
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    debugMode = models.BooleanField(default="False")


class Tux(models.Model):
    code = models.CharField(max_length=3, null=False)
    puzzleId = models.IntegerField()
    puzzle = models.TextField()
    points = models.IntegerField(default=1)


class TuxPickUp(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    tux = models.ForeignKey(Tux, on_delete=models.CASCADE)
    triesLeft = models.IntegerField(default=5)
    solved = models.BooleanField(default=False)


class FinalResult(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    solved = models.IntegerField(default=0)
    tries = models.IntegerField(default=0)

