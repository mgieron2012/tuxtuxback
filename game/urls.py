from django.urls import path
from game.views import Registration, GroupShow, GroupCreate, GroupJoin, GroupLeave, GameShow, GameCreate, PickUpTux,\
    GameResult, Solution, FinalResultView

urlpatterns = [
    path('register', Registration.as_view()),
    path('group/create', GroupCreate.as_view()),
    path('group/join', GroupJoin.as_view()),
    path('group/leave', GroupLeave.as_view()),
    path('group/show', GroupShow.as_view()),
    path('game/show', GameShow.as_view()),
    path('game/create', GameCreate.as_view()),
    path('game/result', GameResult.as_view()),
    path('tux/pickup', PickUpTux.as_view()),
    path('tux/solution', Solution.as_view()),
    path('results', FinalResultView.as_view()),
]
