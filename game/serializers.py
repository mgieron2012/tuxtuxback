from rest_framework import serializers


class GameSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    startTime = serializers.DateTimeField()
    duration = serializers.IntegerField()
    debugMode = serializers.BooleanField()


class ResultSerializer(serializers.Serializer):
    user = serializers.StringRelatedField()
    solved = serializers.IntegerField()
    tries = serializers.IntegerField()
