from datetime import timedelta
from dateutil.parser import isoparse
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from django.db.models import Count, F, Sum
from rest_framework import status
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from game.models import User, GroupMember, Group, Game, TuxPickUp, Tux, FinalResult
from game.serializers import GameSerializer, ResultSerializer
from rest_framework.authtoken.models import Token
import requests


class Registration(APIView):
    def post(self, request):

        token = request.data.get('token', '')
        username = request.data.get('username', '')

        if token == '':
            try:
                user = User.objects.create_user(username=username)
                user.save()
                token = str(Token.objects.create(user=user))
            except IntegrityError:
                return Response(status=status.HTTP_409_CONFLICT, data={'error': 'Username not available'})

        elif username == '':
            username = Token.objects.get(key=token).user.username
        else:
            user = Token.objects.get(key=token).user
            try:
                user.username = username
                user.save()
            except IntegrityError:
                return Response(status=status.HTTP_409_CONFLICT, data={'error': 'Username not available'})

        return Response(data={'token': token, 'username': username}, status=status.HTTP_200_OK)


@permission_classes([IsAuthenticated])
class GroupCreate(APIView):
    def post(self, request):
        try:
            group = Group(name=request.data.get('groupname', ''), password=request.data.get('password', ''))
            group.save()
        except IntegrityError:
            return Response(status=status.HTTP_409_CONFLICT, data={'error': 'Group name not available'})

        try:
            old_group = GroupMember.objects.get(active=True, user=request.user)
            old_group.active = False
            old_group.save()
        except ObjectDoesNotExist:
            pass

        membership = GroupMember(group=group, user=request.user)
        membership.save()

        return Response(status=status.HTTP_200_OK, data={'groupname': group.name})


@permission_classes([IsAuthenticated])
class GroupJoin(APIView):
    def post(self, request):
        try:
            group = Group.objects.get(name=request.data.get('groupname', ''))
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE, data={'error': 'There is no group with given name'})

        if group.password != request.data.get('password', ''):
            return Response(status=status.HTTP_403_FORBIDDEN, data={'error': 'Incorrect password'})

        try:
            old_group = GroupMember.objects.get(active=True, user=request.user)
            old_group.active = False
            old_group.save()
        except ObjectDoesNotExist:
            pass

        membership = GroupMember(group=group, user=request.user)
        membership.save()

        return Response(status=status.HTTP_200_OK, data={'groupname': group.name})


@permission_classes([IsAuthenticated])
class GroupLeave(APIView):
    def post(self, request):

        try:
            membership = GroupMember.objects.get(active=True, user=request.user)
            membership.active = False
            membership.save()
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE, data={'error': 'You are not in any group'})

        return Response(status=status.HTTP_200_OK, data={'groupname': membership.group.name})


@permission_classes([IsAuthenticated])
class GroupShow(APIView):
    def post(self, request):
        try:
            group = GroupMember.objects.get(active=True, user=request.user).group
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE, data={'error': 'You are not in any group'})

        members = GroupMember.objects.filter(group=group, active=True)
        members_names = set(map(lambda mem: mem.user.username, members))
        return Response(status=status.HTTP_200_OK,
                        data={'groupname': group.name, 'members': members_names, 'leader': group.get_leader().username})


@permission_classes([IsAuthenticated])
class GameShow(APIView):
    def post(self, request):
        games = Game.objects.filter(group=request.user.get_group()).order_by('-startTime')
        games_serialized = GameSerializer(games, many=True)
        return Response(status=status.HTTP_200_OK, data={'games': games_serialized.data})


@permission_classes([IsAuthenticated])
class GameResult(APIView):
    def post(self, request):
        try:
            game = Game.objects.get(id=request.data.get('gameId', ''))
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE, data={'error': 'Game not found.'})

        if game.debugMode:
            pickups = TuxPickUp.objects.filter(game=game, solved=True)
            pickups = pickups.values(username=F('user__username'))\
                .annotate(tries=Sum('triesLeft'), points=Count('id')).order_by('-points', '-tries')

            return Response(status=status.HTTP_200_OK, data={'debugMode': True, 'results': pickups})

        else:
            pickups = TuxPickUp.objects.filter(game=game)
            pickups = pickups.values(username=F('user__username')).annotate(points=Count('id')).order_by('-points')

            return Response(status=status.HTTP_200_OK, data={'debugMode': False, 'results': pickups})


@permission_classes([IsAuthenticated])
class GameCreate(APIView):
    def post(self, request):
        if request.user.get_group() is None or request.user.get_group().get_leader() != request.user:
            return Response(status=status.HTTP_403_FORBIDDEN, data={'error': 'Only group leader can create new game'})

        start_time = isoparse(request.data.get('startTime'))
        duration = int(request.data.get('duration'))
        games = Game.objects.filter(group=request.user.get_group())

        for game in games:
            if game.startTime <= start_time <= game.startTime + timedelta(seconds=game.duration) \
                or game.startTime <= start_time + timedelta(seconds=duration) <= game.startTime + \
                    timedelta(seconds=game.duration) or (game.startTime >= start_time and
                    game.startTime + timedelta(seconds=game.duration) <= start_time + timedelta(seconds=duration)):
                return Response(status=status.HTTP_406_NOT_ACCEPTABLE, data={'error': 'Time collide with other game'})

        Game(duration=duration, startTime=start_time, group=request.user.get_group(), debugMode=request.data.get('debugMode')).save()
        return Response(status=status.HTTP_200_OK)


@permission_classes([IsAuthenticated])
class PickUpTux(APIView):
    def post(self, request):
        try:
            game = Game.objects.get(id=request.data.get('gameId', ''))
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE,
                            data={'error': 'Game not found. Try close app and type "play" again'})

        try:
            tux = Tux.objects.get(code=request.data.get('code', ''))
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE, data={'error': 'Code is incorrect'})

        try:
            TuxPickUp.objects.get(tux=tux, game=game, user=request.user)
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE, data={'error': 'Code already used in this game'})
        except ObjectDoesNotExist:
            pass

        TuxPickUp(user=request.user, tux=tux, game=game).save()
        return Response(status=status.HTTP_200_OK, data={'puzzleId': tux.puzzleId, 'puzzle': tux.puzzle})


@permission_classes([IsAuthenticated])
class Solution(APIView):
    def post(self, request):
        try:
            game = Game.objects.get(pk=request.data.get('gameId', ''))
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE,
                            data={'error': 'Game not found. Try close app and type "play" again'})

        try:
            tux = Tux.objects.get(puzzleId=request.data.get('puzzleId', ''))
            pickup = TuxPickUp.objects.get(game=game, user=request.user, tux=tux)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE,
                            data={'error': 'Tux pick up not found. Type camera and try again'})

        if pickup.triesLeft == 0:
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE,
                            data={'error': 'Cannot send more than 5 solutions for one task in one game'})

        try:
            result = FinalResult.objects.get(user=request.user)
        except ObjectDoesNotExist:
            result = FinalResult(user=request.user).save()

        solution = request.data.get('solution', '')
        pickup.triesLeft -= 1
        pickup.save()

        try:
            r = requests.post('http://127.0.0.1:2137/game/command', data={'commandIdx': tux.puzzleId, 'answer': solution})
        except requests.exceptions.ConnectionError:
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE,
                            data={'error': 'Spróbuj ponownie. Jeśli nic się nie zmieni, znajdź Michała Borucha z 4B i'
                                           ' poproś go ładnie, żeby sprawdził co się dzieje z jego serwerem,'
                                           ' który bezinteresownie udostępnił nam na potrzeby tego projektu. Wykonać'})

        if r.status_code == 200:
            try:
                TuxPickUp.objects.get(user=request.user, solved=True, tux=tux)
            except ObjectDoesNotExist:
                result.solved += 1
                result.tries += 1
                result.save()

            pickup.solved = True
            pickup.save()
            return Response(status=status.HTTP_200_OK)

        if r.status_code == 406:
            try:
                TuxPickUp.objects.get(user=request.user, solved=True, tux=tux)
            except ObjectDoesNotExist:
                result.tries += 1
                result.save()

            return Response(status=status.HTTP_400_BAD_REQUEST, data={'error': 'Solution is incorrect'})

        pickup.triesLeft += 1
        pickup.save()

        return Response(status=status.HTTP_501_NOT_IMPLEMENTED, data={'error': 'Something went wrong. Try again'})


@permission_classes([IsAuthenticated])
class FinalResultView(APIView):
    def post(self, request):
        all_results = FinalResult.objects.all().order_by('-solved', 'tries')
        all_results_serialized = ResultSerializer(all_results, many=True)
        return Response(status=status.HTTP_200_OK, data={'results': all_results_serialized.data})
